# Build

```
git clone https://gitlab.azka.li/l4t-community/ubtouch/nintendo_switch
cd nintendo_switch
./build.sh
```

# Flash

- Create GPT
- Create 3 partitions named: `vendor` `APP` `UDA`
- format UDA as ext4
- Flash vendor to `vendor` and `rootfs` to `APP`
