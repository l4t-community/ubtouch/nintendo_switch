FROM gitlab-registry.azka.li/l4t-community/gnu-linux/jet-factory:master

RUN apt update && apt install -y ccache e2fsprogs repo simg2img \
  git-core git-lfs gnupg flex bison gperf build-essential \
  zip curl zlib1g-dev gcc-multilib g++-multilib \
  x11proto-core-dev libx11-dev lib32z-dev libncurses5 \
  libgl1-mesa-dev libxml2-utils xsltproc unzip liblz4-tool libssl-dev \
  libc++-dev libevent-dev flatbuffers-compiler libflatbuffers1 \
  openssl libssl-dev rsync sudo libwxgtk3.0-gtk3-dev \
  bc curl gperf imagemagick lib32ncurses5-dev \
  lib32readline-dev lib32z1-dev libelf-dev libncurses5 libncurses5-dev \
  libsdl1.2-dev libssl-dev libxml2 lzop pngcrush schedtool squashfs-tools \
  xsltproc zip zlib1g-dev

RUN git config --global user.email "you@example.com"
RUN git config --global user.name "Your Name"
RUN git config --global color.ui  true

RUN curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/bin/repo
RUN chmod +x /usr/bin/repo

ENV USE_CCACHE=1
ENV CCACHE_EXEC=/usr/bin/ccache
ENV CCACHE_DIR=/build/downloads/ccache

ENTRYPOINT []
SHELL ["/bin/bash", "-c"]
CMD cd /build && ./build/build.sh -b .
