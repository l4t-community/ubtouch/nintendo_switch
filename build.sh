#!/bin/bash
set -xe

if [[ $1 = "docker" ]]; then
	docker build -t nx .
	docker run --privileged --rm -it -v "$PWD":/build nx
	exit
fi

[ -d build ] || git clone https://gitlab.com/Azkali/halium-generic-adaptation-build-tools -b halium-12-nx build
./build/build.sh -b .
ccache -M 100G
